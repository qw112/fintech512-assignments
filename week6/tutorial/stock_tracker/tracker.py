import requests
from flask import Flask, render_template, request, redirect, url_for, g
from datetime import datetime
from stock_tracker import config
from stock_tracker.db import get_db
from flask import Blueprint
from stock_tracker.auth import login_required
bp = Blueprint("tracker", __name__)


# helper function to get the current stock price
def get_stock_price(symbol):
    response = requests.get(f'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={symbol}&interval=5min&apikey={config.api_key}')
    data = response.json()
    print(data)
    time = data["Meta Data"]["3. Last Refreshed"]
    tsdata = data["Time Series (5min)"]
    prices = tsdata[time]
    return float(prices["4. close"])

@bp.route('/', methods=['GET', 'POST'])
@login_required
def index():
    if request.method == 'POST':
        # add a new stock to the database
        symbol = request.form['symbol']
        tracking_price = float(request.form['tracking_price'])
        print(tracking_price)
        shares = int(request.form['num_shares'])
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        db = get_db()
        print(symbol, tracking_price, shares, now)
        db.execute('INSERT INTO stocks (symbol, tracking_price, shares, date_added) VALUES (?, ?, ?, ?)', (symbol, tracking_price, shares, now))
        db.commit()
        return redirect(url_for('index'))
    else:
        # display the list of stocks
        db = get_db()
        cursor = db.execute('SELECT * FROM stocks')
        rows = cursor.fetchall()
        stocks = []
        for row in rows:
            id = row['id']
            symbol = row['symbol']
            tracking_price = row['tracking_price']
            shares = row['shares']
            date_added = row['date_added']
            current_price = get_stock_price(symbol)
            percent_change = (current_price - tracking_price) / tracking_price * 100
            stocks.append({
                'id':id,
                'symbol': symbol,
                'tracking_price': tracking_price,
                'shares': shares,
                'date_added': date_added,
                'current_price': current_price,
                'percent_change': percent_change
            })
            print(stocks)
        return render_template('index.html', stocks=stocks)

@bp.route('/delete/<int:id>', methods=['POST'])
def delete(id):
    # delete the stock from the database
    db = get_db()
    db.execute('DELETE FROM stocks WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('index'))