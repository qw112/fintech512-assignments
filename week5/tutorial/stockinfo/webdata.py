#%%
import requests
import datetime
from config import api_key
import pandas as pd
#%%
def get_current_data(api_key,symbol):
    # store a time series data
    response = requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={}&interval=5min&apikey={}'.format(symbol,api_key))
    data = response.json()
    basic_info = dict()
    time = data["Meta Data"]["3. Last Refreshed"]
    tsdata = data["Time Series (5min)"]
    prices = tsdata[time]
    basic_info['current_prices'] = prices["4. close"]
    basic_info['open'] = prices["1. open"]
    basic_info['volume'] = prices["5. volume"]
    
    time = datetime.datetime.strptime(time,'%Y-%m-%d %H:%M:%S')
    prev_time = 0
    while prev_time not in tsdata:
        if prev_time == 0:
            prev_time = time-datetime.timedelta(minutes=5)
            prev_time = prev_time.strftime('%Y-%m-%d %H:%M:%S')
        else:
            prev_time = datetime.datetime.strptime(prev_time,'%Y-%m-%d %H:%M:%S')
            prev_time = prev_time-datetime.timedelta(minutes=5)
            prev_time = prev_time.strftime('%Y-%m-%d %H:%M:%S')


    basic_info['prev_close'] = tsdata[prev_time]["4. close"]

    return basic_info
#%%
def get_fundamental_data(api_key,symbol):
    basic_info = dict()
    response = requests.get('https://www.alphavantage.co/query?function=OVERVIEW&symbol={}&apikey={}'.format(symbol,api_key))
    data = response.json()
    basic_info['StockSymbol'] = symbol
    basic_info['CompanyName'] = data['Name']
    basic_info['Sector'] = data['Sector']
    basic_info['Industry'] = data['Industry']
    basic_info['MarketCap'] = data["MarketCapitalization"]
    basic_info['PE'] = data['PERatio']
    basic_info['EPS'] = data['EPS']
    basic_info['Exchange'] = data['Exchange']
    basic_info['DividendPerShare'] = data["DividendPerShare"]
    basic_info['DividendYield'] = data["DividendYield"]
    basic_info['52weekhigh'] = data['52WeekHigh']
    basic_info['52weeklow'] = data['52WeekHigh']

    current_info = get_current_data(api_key,symbol)

    basic_info.update(current_info)

    return basic_info
#%%
def get_news_data(api_key,symbol):
    # store new papage

    response = requests.get('https://www.alphavantage.co/query?function=NEWS_SENTIMENT&symbol={}&apikey={}'.format(symbol,api_key))
    data = response.json()
    data = data['feed']
    data = data[0:5]
    basic_info = dict()

    for i in range(5):
        basic_info[i] = dict()
        basic_info[i]['title'] = data[i]['title']
        basic_info[i]['url'] = data[i]['url']
        basic_info[i]['summary'] = data[i]['summary']

    return basic_info
#%%
def get_stock_ts(api_key,symbol):
    response = requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={}&apikey={}'.format(symbol,api_key))
    data = response.json()
    current_time = data["Meta Data"]["3. Last Refreshed"][:10]
    current_time = datetime.datetime.strptime(current_time,'%Y-%m-%d')
    pst_time = current_time - datetime.timedelta(days=365)
    pst_time = datetime.datetime.strftime(pst_time,'%Y-%m-%d')
    basic_info = dict({'X':[],'Y':[]})

    ts_data = data["Time Series (Daily)"]

    for i in pd.date_range(pst_time,current_time,freq='D'):
        n = datetime.datetime.strftime(i,'%Y-%m-%d')
        if n in ts_data:
            basic_info['X'].append(n)
            prices = ts_data[n]["4. close"]
            basic_info['Y'].append(prices)

    return basic_info

#%%
def get_stock_data(api_key,symbol):
    basic_info = get_fundamental_data(api_key,symbol)
    stock_news = get_news_data(api_key,symbol)
    stock_ts = get_stock_ts(api_key,symbol)
    return basic_info,stock_news,stock_ts

# %%
