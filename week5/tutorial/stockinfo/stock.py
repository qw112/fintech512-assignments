from flask import Flask, render_template, request
from webdata import get_stock_data
import plotly.graph_objs as go
import plotly.utils
import json
from config import api_key

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    basic_info = {}
    stock_news = {}
    stock_ts = {}
    
    if request.method == 'POST':
        symbol = request.form['stock_symbol']
        basic_info,stock_news,stock_ts = get_stock_data(api_key,symbol)

    return render_template('stockpage.html',basic_info=basic_info,stock_news=stock_news,stock_ts=stock_ts)




if __name__ == '__main__':
    app.run()